export const PRODUCTS_FETCH_SUCCESS = 'products_fetch_success';
export const PRODUCTS_FETCH_PENDING = 'products_fetch_pending';
export const PRODUCTS_CREATE_SUCCESS = 'products_create_success';
export const PRODUCTS_UPDATE_SUCCESS = 'products_update_success';
export const PRODUCTS_DELETED_SUCCESS = 'products_delete_success';
export const PRODUCTS_CREATE_FORM = 'products_create_form';