import axios from 'axios';
import {
  PRODUCTS_FETCH_SUCCESS,
  PRODUCTS_CREATE_SUCCESS,
  PRODUCTS_DELETED_SUCCESS,
  PRODUCTS_FETCH_PENDING,
} from './types';

export const productsListFetch = () => {
  return (dispatch) => {
    dispatch({ type: PRODUCTS_FETCH_PENDING });
    axios.get('http://localhost:8080/v1/api/product/all')
    .then( (response) => {
      dispatch({ type: PRODUCTS_FETCH_SUCCESS, payload: response.data });    
    })
    .catch(error => {
      console.log("ERROR: ",error);
    })
  }
}

export const productCreate = (product) => {
  return (dispatch) => {
    axios.post('http://localhost:8080/v1/api/product/save', product)
    .then(() => {
      console.log("product created");
      dispatch({ type: PRODUCTS_CREATE_SUCCESS, payload: product });
    })
    .catch(error => console.log("ERROR: ", error));
  }
}

export const productUpdate = (product) => {
  return(dispatch) => {
    axios.put('http://localhost:8080/v1/api/product/update', product)
    .then(() => {
      console.log("product updated");
    })
    .catch(error => console.log("ERROR: ", error));
  }
}

export const productDelete = (productId) => {
  return (dispatch) => {
    axios.delete(`http://localhost:8080/v1/api/product/delete?idProducto=${productId}`)
    .then(() => {
      console.log(`product with id ${productId} was deleted`);
      dispatch({ type: PRODUCTS_DELETED_SUCCESS, payload: productId });
    })
    .catch(error => console.log("ERROR: ", error));
  }
}