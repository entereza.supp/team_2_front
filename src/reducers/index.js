import { combineReducers } from "redux";
import ProductsReducer from "./ProductsReducer";
import ProductFormReducer from "./ProductFormReducer";

export default combineReducers({
  products: ProductsReducer,
  productForm: ProductFormReducer
});