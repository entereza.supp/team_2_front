import {
  PRODUCTS_FETCH_SUCCESS,
  PRODUCTS_CREATE_SUCCESS,
  PRODUCTS_DELETED_SUCCESS,
  PRODUCTS_FETCH_PENDING,
} from '../actions/types';

const INITIAL_STATE = {
  data: [],
  pending: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PRODUCTS_FETCH_SUCCESS:
      return {
        ...state,
        data: action.payload,
        pending: false
      }
    case PRODUCTS_CREATE_SUCCESS:
      return {
        ...state, 
        data: [...state.data, action.payload] 
      }
    case PRODUCTS_DELETED_SUCCESS:
      return { 
        ...state, data: state.data.filter(({idproductos}) => idproductos !== action.payload) 
      };
    case PRODUCTS_FETCH_PENDING:
      return {
        ...state, 
        pending: true
      }  
    default:
      return state;
  }
}