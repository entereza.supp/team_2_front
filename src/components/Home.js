import { Fragment, useState, useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { productsListFetch, productDelete } from '../actions/ProductsActions'
import CelList from "./CelList";

 const Home = () => {
  const navigate = useNavigate();

  const { data: products } = useSelector((store) => store.products);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(productsListFetch()); 
  }, [products.length]);

  const createProduct = () => {
    navigate('/createCel');
  }

  const updateProduct = (product) => {
    navigate('/updateCel', {state: product});
  }

  const deleteProduct = (id) => {
    dispatch(productDelete(id));
  }

  return (
    <Fragment>
      <button type="button" className="btn btn-primary mt-4" onClick={() => createProduct()}>Nuevo +</button>
      <CelList products = {products} updateProduct = {updateProduct} deleteProduct = {deleteProduct} />
    </Fragment>
  );
}

export default Home;