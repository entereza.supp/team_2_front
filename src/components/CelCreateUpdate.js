import { useSelector, useDispatch } from "react-redux";
import { productCreate, productUpdate } from "../actions/ProductsActions";
import CelForm from "./CelForm";

const CelCreateUpdate = (props) => {
  const newProduct = useSelector((state) => state.productForm);
  const dispatch = useDispatch();

  const saveProduct = ({cantidad, descripcion, direccion_imagen, idproductos, nombre, precio_unidad}) => {
    !idproductos ? dispatch(productCreate({cantidad, descripcion, direccion_imagen, nombre, precio_unidad}))
    : dispatch(productUpdate({cantidad, descripcion, direccion_imagen, idproductos, nombre, precio_unidad}));
  }

  const product = props.product ? props.product : newProduct;
  return <CelForm saveProduct = {saveProduct} product = {product} />
}

export default CelCreateUpdate;