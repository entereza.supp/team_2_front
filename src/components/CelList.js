const CelList = ({products, updateProduct, deleteProduct}) => {
  return(
    <table className="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nombre</th>
          <th scope="col">Descripcion</th>
          <th scope="col">Precio</th>
          <th scope="col">Cantidad</th>
          <th scope="col">Acciones</th>
        </tr>
      </thead>
      <tbody>
        {
          products.map((element, i) => {
            return(
              <tr key={i}>
                <th scope="row">{i + 1}</th>
                <td>{element.nombre}</td>
                <td>{element.descripcion}</td>
                <td>{element.precio_unidad} Bs.</td>
                <td>{element.cantidad}</td>
                <td>
                  <button className="btn btn-warning m-1" onClick={() => updateProduct(element)}>Editar</button>
                  <button className="btn btn-danger m-1" onClick={() => deleteProduct(element.idproductos)}>Borrar</button>
                </td>
              </tr>
            );
          })
        }
      </tbody>
    </table>
  );
}

export default CelList;