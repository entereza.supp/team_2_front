import { Formik , ErrorMessage } from 'formik';
import * as yup from 'yup';
import { setLocale } from 'yup';
import {useNavigate} from 'react-router-dom';

setLocale({
  mixed: {
    default: 'No es valido',
  },
  number: {
    min: 'Debe ser mayor a ${min}',
  },
  string: {
    min: 'Ingrese al menos ${min} caracteres'
  }
});

const ProductSchema = yup.object({
  nombre: yup.string()
    .label('nombre del producto')
    .required('Debes ingresar el ${label}.')
    .max(45)
    .min(3)
    .trim(),
  descripcion: yup.string()
    .label('descripcion')
    .required('Debes ingresar la ${label}.')
    .max(45)
    .min(3)
    .trim(),
  precio_unidad: yup.string()
    .label('precio')
    .required('Debes ingresar una ${label} del equipo')
    .min(1)
    .max(45)
    .trim(),
  cantidad: yup.string()
    .label('cantidad')
    .required('Debes ingresar la ${label}')
    .min(1)
    .max(45)
    .trim(),
  direccion_imagen: yup.string()
    .label('ruta de imagen')
    .required('Debes ingresar una ${label}')
    .min(1)
    .trim(),
});

const CelForm = ({ product, saveProduct }) => {
  const navigate = useNavigate();
  const { cantidad, descripcion, direccion_imagen, idproductos, nombre, precio_unidad } = product

  return (
    <Formik
      initialValues = {{cantidad, descripcion, direccion_imagen, idproductos, nombre, precio_unidad}}
      validationSchema = {ProductSchema}
      enableReinitialize= {true}
      onSubmit= {(values, actions) => {
        actions.resetForm();
        saveProduct(values);
        navigate('/');
      }}
    >
      {(props) => (
        <form className="m-3" onSubmit={props.handleSubmit} >
          <label className="form-label">Nombre del Producto</label>
          <input
            className="form-control"
            id="nombre"
            type="text"
            name="nombre"
            onChange={props.handleChange}
            value={props.values.nombre}
            onBlur={props.handleBlur('nombre')}
          />
          <p className="text-danger">{props.errors.nombre && props.touched.nombre && props.errors.nombre}</p>
          <label className="form-label">Descripcion</label>
          <input
            className="form-control"
            type="text"
            id="descripcion"
            name="descripcion"
            onChange={props.handleChange}
            value={props.values.descripcion}
          />
          <p className="text-danger">{props.errors.descripcion && props.touched.descripcion && props.errors.descripcion}</p>
          <label className="form-label">Precio</label>
          <input
            className="form-control"
            type="text"
            id="precio_unidad"
            name="precio_unidad"
            onChange={props.handleChange}
            value={props.values.precio_unidad}
          />
          <p className="text-danger">{props.errors.precio_unidad && props.touched.precio_unidad && props.errors.precio_unidad}</p>
          <label className="form-label">Cantidad</label>
          <input
            className="form-control"
            type="text"
            id="cantidad"
            name="cantidad"
            onChange={props.handleChange}
            value={props.values.cantidad}
          />
          <p className="text-danger">{props.errors.cantidad && props.touched.cantidad && props.errors.cantidad}</p>
          <label className="form-label">Imagen</label>
          <input
            className="form-control"
            type="text"
            id="direccion_imagen"
            name="direccion_imagen"
            onChange={props.handleChange}
            value={props.values.direccion_imagen}
          />
          <p className="text-danger">{props.errors.direccion_imagen && props.touched.direccion_imagen && props.errors.direccion_imagen}</p>
          <button className="btn btn-primary mt-2" type="submit">
            Guardar
          </button>
        </form>
      )}
    </Formik>
  )
}

export default CelForm;