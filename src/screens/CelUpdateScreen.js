import CelCreateUpdate from "../components/CelCreateUpdate";
import {useLocation} from 'react-router-dom';

const CelUpdateScreen = () => {
  const product = useLocation().state;
  return <CelCreateUpdate product = {product} />
}

export default CelUpdateScreen;