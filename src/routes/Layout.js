import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import HomeScreen from '../screens/HomeScreen';
import CelCreateScreen from '../screens/CelCreateScreen';
import CelUpdateScreen from '../screens/CelUpdateScreen';
import Login from '../components/Auth/Login';
import SignUp from '../components/Auth/SignUp';

const Layout = () => {
  return(
    <Router>
      <div>
        <Routes>
          <Route exact path="/" element={ <HomeScreen/>} />
          <Route exact path="/createCel" element={ <CelCreateScreen/>} />
          <Route exact path="/updateCel" element={ <CelUpdateScreen /> } />
          <Route exact path="/login" element={ <Login/>} />
          <Route exact path="/SignUp" element={ <SignUp/>} />
        </Routes>
      </div>
    </Router>
  );
}

export default Layout;